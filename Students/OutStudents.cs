﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Students
{
    public partial class OutStudents : Form
    {
        FormState formState;
        public OutStudents(FormState formState, Student student = null)
        {
            InitializeComponent();
            this.formState = formState;
            switch (formState)
            {
                case FormState.ADD:
                    {
                        EditFIO.ReadOnly = false;
                        EditNum.ReadOnly = false;
                        comboBox1.DropDownStyle = ComboBoxStyle.DropDown;
                        comboBox2.DropDownStyle = ComboBoxStyle.DropDown;
                        comboBox3.DropDownStyle = ComboBoxStyle.DropDown;
                        comboBox4.DropDownStyle = ComboBoxStyle.DropDown;
                        Text = "Add student";
                        break;
                    }
                case FormState.EDIT:
                    {
                        EditNum.ReadOnly = true;
                        EditNum.Text = Convert.ToString(student.Num);
                        EditFIO.Text = student.Name;
                        comboBox1.Text = student.Exam[0].ToString();
                        comboBox2.Text = student.Exam[1].ToString();
                        comboBox3.Text = student.Exam[2].ToString();
                        comboBox4.Text = student.Exam[3].ToString();
                        Text = "Edit student";
                        break;
                    }

                case FormState.DELETE:
                case FormState.SEARCH:
                    {
                        EditNum.ReadOnly = false;
                        EditFIO.ReadOnly = true;
                        comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                        comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
                        comboBox3.DropDownStyle = ComboBoxStyle.DropDownList;
                        comboBox4.DropDownStyle = ComboBoxStyle.DropDownList;
                        Text = "Search student";
                        break;
                    }

                case FormState.DISPLAY:
                    {
                        EditNum.ReadOnly = true;
                        EditFIO.Text = student.Name;
                        comboBox1.Text = student.Exam[0].ToString();
                        comboBox2.Text = student.Exam[1].ToString();
                        comboBox3.Text = student.Exam[2].ToString();
                        comboBox4.Text = student.Exam[3].ToString();

                        EditFIO.ReadOnly = true;
                        comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                        comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
                        comboBox3.DropDownStyle = ComboBoxStyle.DropDownList;
                        comboBox4.DropDownStyle = ComboBoxStyle.DropDownList;
                        Text = "Show student";
                        break;
                    }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
           if (EditFIO.Text != "" && EditNum.Text != "")
            {
                this.Close();
            }
           if(formState != FormState.ADD)
            {
                this.Close();
            }
           
        }

        public int getKey()
        {
            if (EditNum.Text != "")
            {
                return Convert.ToInt32(EditNum.Text);
            } else
            {
                return -1;
            }

        }

        public Student GetInfo()
        {
            ArrayList marks = new ArrayList();
            marks.Add(comboBox1.SelectedItem);
            marks.Add(comboBox2.SelectedItem);
            marks.Add(comboBox3.SelectedItem);
            marks.Add(comboBox4.SelectedItem);
            return new Student(EditFIO.Text, Convert.ToInt32(EditNum.Text), marks);
        }       

        private void EditFIO_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letter = e.KeyChar;
            if (!Char.IsLetter(letter) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;   //если не буква, то необрабатываем(игнорируем)
            } 
        }

        private void EditNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Delete)    //если не цифра, то необрабатываем(игнорируем)
            {
                e.Handled = true;
            } 
        }
    }
}
