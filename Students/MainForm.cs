﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Students
{
    public partial class MainForm : System.Windows.Forms.Form
    {
        private OutStudents form;
        private HashTable hashtable;
        private Student student;
        private string path = "";


        public MainForm()  //создание хэштаблицы, элементов формы
        {
            InitializeComponent();
            hashtable = new HashTable(50);
        }


        private void Redraw()        //вывод всех студентов
        {
            TableStudent.Rows.Clear();
            List<Student> list = hashtable.GetData();
            TableStudent.RowCount = list.Count;
            for (int i = 0; i < TableStudent.RowCount; i++)
            {
                TableStudent.Rows[i].Cells[0].Value = list[i].Name;
                TableStudent.Rows[i].Cells[1].Value = list[i].Num;
                TableStudent.Rows[i].Cells[2].Value = Student.countFailMark(list[i].Exam);
            }
        }

        private void studentToolStripMenuItem1_Click_1(object sender, EventArgs e) //вывод всех студентов в хэштаблице
        {
            Redraw();
        }

        private void studentToolStripMenuItem_Click(object sender, EventArgs e)   //добавление данных
        {
            form = new OutStudents(FormState.ADD);
            form.ShowDialog();
            student = form.GetInfo();
            if (hashtable.Add(student))
            {
                Redraw();
            } else
            {
                MessageBox.Show("Данные не добавлены");
            }
        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)   //поиск студента
        {
            form = new OutStudents(FormState.SEARCH);
            form.ShowDialog();
            student = findNormal(form);
            if (student != null)
            {
                OutStudents formView = new OutStudents(FormState.DISPLAY, student);
                formView.ShowDialog();
            }
            else
            {
                MessageBox.Show("Студент не найден");
            }
        }

        private Student findNormal(OutStudents form)
        {
            int k = form.getKey();
            if(k > 0)
            {
                return hashtable.Find(k);
            } else
            {
                return null;
            }
        }

        private void editToolStripMenuItem1_Click(object sender, EventArgs e)  //исправление данных 
        {
            form = new OutStudents(FormState.SEARCH);
            form.ShowDialog();
            student = findNormal(form);
            if(student != null)
            {
                OutStudents editForm = new OutStudents(FormState.EDIT, student);
                editForm.ShowDialog();
                hashtable.Delete(student.Num);
                hashtable.Add(editForm.GetInfo());
                Redraw();
            } else
            {
                MessageBox.Show("Студент не найден");
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)  //удаление данных
        {
            form = new OutStudents(FormState.SEARCH);
            form.ShowDialog();
            student = findNormal(form);
            if(student != null)
            {
                hashtable.Delete(student.Num);
                Redraw();
            } else
            {
                MessageBox.Show("Студент не найден");
            }                             
        }

        private void createFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (path == "")
            {
                SaveToFile();
            }  else
            {
                hashtable.HashInputFile(path);
            }
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveToFile();
        }

        private void openFileToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            hashtable.HashOutputFile(openFileDialog1.FileName);
        }

        private void SaveToFile()
        {
            saveFileDialog1.ShowDialog();
            path = saveFileDialog1.FileName;
            hashtable.HashInputFile(path);
        }      
    }
}
