﻿using Students;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Students
{
    public class HashTable
    {
        private Elem[] Table { get; }
        public int Size { get; }
        public HashTable(int size = 100) // Конструктор
        {
            Size = size;
            Table = new Elem[size];
        }
        public bool Add(Student student)
        {
            if (Find(student.Num) != null)
            {
                return false;
            }
            int index = student.Num % Size;
            if (Table[index] == null || Table[index].Deleted) // Добавление в таблицу
            {
                Table[index] = new Elem()
                {
                    Student = student
                };
                return true;
            }
            for (int i = index + 1; i < Size; i++) // Метод решения коллизии
            {
                if (Table[i] == null || Table[i].Deleted)
                {
                    Table[i] = new Elem()
                    {
                        Student = student
                    };
                    return true;
                }
            }
            for (int i = 0; i < index; i++)
            {
                if (Table[i] == null || Table[i].Deleted)
                {
                    Table[i] = new Elem()
                    {
                        Student = student
                    };
                    return true;
                }
            }
            return false;
        }
        public Student Find(int key)
        {
            int index = key % Size;
            if (Table[index] == null)
            {
                return null;
            }
            if (!Table[index].Deleted && Table[index].Student.Num == key)
            {
                return Table[index].Student;
            }
            for (int i = index + 1; i < Size; i++)
            {
                if (Table[i] == null)
                {
                    return null;
                }
                if (!Table[i].Deleted && Table[i].Student.Num == key)
                {
                    return Table[i].Student;
                }
            }
            for (int i = 0; i < index; i++)
            {
                if (Table[i] == null)
                {
                    return null;
                }
                if (!Table[i].Deleted && Table[i].Student.Num == key)
                {
                    return Table[i].Student;
                }
            }
            return null;
        }
        public void Delete(int key)
        {
            int index = key % Size;
            if (Table[index] == null)
            {
                return;
            }
            if (!Table[index].Deleted && Table[index].Student.Num == key)
            {
                Table[index].Deleted = true;
                return;
            }
            for (int i = index + 1; i < Size; i++)
            {
                if (Table[i] == null)
                {
                    return;
                }
                if (!Table[i].Deleted && Table[i].Student.Num == key)
                {
                    Table[i].Deleted = true;
                    return;
                }
            }
            for (int i = 0; i < index; i++)
            {
                if (Table[i] == null)
                {
                    return;
                }
                if (!Table[i].Deleted && Table[i].Student.Num == key)
                {
                    Table[i].Deleted = true;
                    return;
                }
            }
        }


        public void Clear()
        {
            for (int i = 0; i < Size; i++)
            {
                Table[i] = null;
            }
        }

        public List<Student> GetData()
        {
            List<Student> list = new List<Student>();
            foreach (Elem e in Table)
            {
                if (e != null && !e.Deleted)
                {
                    list.Add(e.Student);
                }
            }
            return list;
        }

        public override string ToString()
        {
            string result = "{";
            for (int i = 0; i < Size; i++)
            {
                if (Table[i] != null && Size > 0)
                {
                    result += result == "{" ? $"\n {i}: {Table[i]}" : $",\n {i}: {Table[i]}";
                }
            }

            result += "\n}";
            return result;
        }

        //добавление  в файл
        public bool HashInputFile(string path)
        {
            FileStream fileStream = null;
            StreamWriter writer = null;
            try
            {
                fileStream = new FileStream(path, FileMode.Open);
                writer = new StreamWriter(fileStream);
                writer.WriteLine(ToString());
            }
            catch
            {
                return false;

            }
            finally
            {
                if (fileStream != null)
                {
                    writer.Close();
                    fileStream.Close();
                }
            }
            return true;
        }


        //чтение из файла
        public bool HashOutputFile(string path)
        {
            FileStream fileStream = null;
            StreamReader reader = null;
            Student student = new Student();
            try
            {
                fileStream = new FileStream(path, FileMode.Open);
                reader = new StreamReader(fileStream);
                string line;
                int k = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    switch (k)
                    {
                        case 0:
                            {
                                student.Name = line;
                                break;
                            }
                        case 1:
                            {
                                student.Num = Convert.ToInt32(line);
                                break;
                            }
                        case 3:
                            {
                                ArrayList arrayList = new ArrayList();
                                foreach (char c in line)
                                {
                                    if (c != ' ')
                                    {
                                        arrayList.Add(c);
                                    }
                                }
                                student.Exam = arrayList;
                                break;
                            }
                    }
                    k = k == 3 ? 0 : k++;
                }

            }
            catch
            {
                MessageBox.Show("Файл не найден");
                return false;
            }
            finally
            {
                if (fileStream != null)
                {
                    reader.Close();
                    fileStream.Close();
                }
            }

            return true;
        }

        class Elem
        {
            public Student Student { get; set; }
            public bool Deleted { get; set; } = false;

        }
    }
}